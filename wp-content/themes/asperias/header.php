<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asperias
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body id="home" class="wide" <?php body_class(); ?>>
<!-- WRAPPER -->
<main id="home-two" class="wrapper">
    <!-- HEADER -->
    <header class="header-two overlay">
        <div class="top-bar">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="list-inline custom-link">
                        <?php if (get_field('telefonnummer', 'option')) : ?>
                            <li>
                                <span> <i class="icon_phone"></i></span>
                                <span> Call Us: <b><?php the_field('telefonnummer', 'option'); ?></b>
                            </span>
                            </li>
                        <?php endif; ?>
                        <?php if (get_field('whatsapp', 'option')) : ?>
                            <li>
                                <span><i class="fa fa-whatsapp"></i></span>
                                <span> WhatsApp: <b><?php the_field('whatsapp', 'option'); ?></b>
                            </span>
                            </li>
                        <?php endif; ?>
                        <?php if (get_field('email', 'option')) : ?>
                            <li>
                                <span> <i class="icon_mail"></i> Email: </span>
                                <a href="mailto:<?php the_field('email', 'option'); ?>"> <?php the_field('email', 'option'); ?> </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="list-inline top-nav">
                        <?php wp_nav_menu(array('theme_location' => 'menu-2')); ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="header-wrap">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-5 logo-wrap">
                    <?php
                    if (has_custom_logo()) :
                        the_custom_logo();
                    else :
                        if (is_front_page() && is_home()) :
                            ?>
                            <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                                      rel="home"><?php bloginfo('name'); ?></a></h1>
                        <?php
                        else :
                            ?>
                            <p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>"
                                                     rel="home"><?php bloginfo('name'); ?></a></p>
                        <?php
                        endif;
                    endif; ?>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-7 navigation-menu">
                    <div class="top-elements">
                        <span class="nav-trigger open icon_menu"></span>
                        <div class="search-form-wrap">
                            <span class="icon-magnifier search-icon"></span>
                            <?php echo get_product_search_form(); ?>
                        </div>
                        <div class="header-cart-wrap">
                            <div class="header-cart">
                                <a class="cart-customlocation">
                                    <i class=" cart-icon icon-handbag icons"></i> <span
                                            class="count"> <?php echo sprintf(_n('%d', '%d', WC()->cart->get_cart_contents_count()), WC()->cart->get_cart_contents_count()); ?>
                                    </span>
                                </a>
                            </div>
                            <div class="cart-popup">
                                <div class="cart-box">
                                    <ol class="cart-list woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr($args['list_class']); ?>">
                                        <?php woocommerce_mini_cart(); ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-bar">
                        <span class="nav-trigger close-icon icon_close"></span>
                        <?php main_menu(); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- CONTENT AREA -->
    <article class="page-body">
