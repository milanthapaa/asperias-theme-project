<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asperias
 */

?>

</article>
<!-- FOOTER -->
<footer class="footer-seven">
    <section class="footer-bg">
        <div class="footer-widget">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 widget-wrap address-ftr">
                        <h2 class="widget-title"> KONTAKT & FILIALEN </h2>
                        <?php the_field('fuszeile_kontakt_&_filials', 'option') ?>
                    </div>
                    <div class="col-md-4 col-sm-4 widget-wrap">
                        <h2 class="widget-title"> INFORMATION </h2>
                        <ul class="twitter-widget">
                            <?php while (have_rows('fuszeile_links', 'option')): the_row(); ?>
                                <?php $link = get_sub_field('link'); ?>
                                <li>
                                    <a href="<?php echo get_permalink($link); ?>"><?php echo get_the_title($link); ?> </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 widget-wrap">
                        <h2 class="widget-title"> GET TOUCH WITH US </h2>
                        <div class="social-media">
                            <h2 class="title-2 gray-color"> follow us on </h2>
                            <ul class="social-icon">
                                <?php if (get_field('facebook', 'option')) : ?>
                                    <li><a class="social_facebook"
                                           href="<?php the_field('facebook', 'option'); ?>" target="_blank"></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_field('instagram', 'option')) : ?>
                                    <li><a class="social_instagram"
                                           href="<?php the_field('instagram', 'option'); ?>" target="_blank"></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_field('twitter', 'option')) : ?>
                                    <li><a class="social_twitter"
                                           href="<?php the_field('twitter', 'option'); ?>" target="_blank"></a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="ftr-payment-icon">
                            <h2 class="title-2 gray-color"> Our Payment Partner </h2>
                            <ul>
                                <?php while (have_rows('zahlungspartner_bilder', 'option')): the_row(); ?>
                                    <?php $image = get_sub_field('image'); ?>
                                    <li><img src="<?php echo $image; ?>"></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="copy-rights">
        <div class="container">
            <?php the_field('fuszeile_copyright', 'option'); ?>
        </div>
    </section>
</footer>
<!-- /FOOTER -->
<div id="to-top" class="to-top"><i class="arrow_carrot-up"></i></div>
</main>

<?php wp_footer(); ?>
<script>
    jQuery(function ($) {
        $('.navbar .dropdown').hover(function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function () {
            location.href = this.href;
        });

    });
</script>
</body>
</html>
