<?php
/*=========================================
   Home page information section 2 shortcode
 =========================================*/
function get_bestseller_product()
{
    $args = array(
        'post_type' => 'product',
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num',
        'posts_per_page' => 10,
    );
    $loop = new WP_Query($args);
    while ($loop->have_posts()) : $loop->the_post();
        global $product; ?>
        <div class="item">
            <div class="product-item">
                <div class="product-image">
                    <a href="<?php the_permalink(); ?>" class="img">
                        <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>"/>
                    </a>
                </div>
                <div class="product-content">
                    <h2 class="product-title"><a
                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata();
}

add_shortcode('bestseller_product', 'get_bestseller_product');

