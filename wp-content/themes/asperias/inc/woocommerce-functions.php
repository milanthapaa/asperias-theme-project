<?php
/**
 * This file includes all the code to override woocommerce default markup via hooks
 */

//Removing WooCommerce Before and After Wrapper
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

//Adding WooCommerce Custom Before Wrapper
function my_woocommerce_wrapper_start()
{
    if (!is_product()) {
        echo "<section class='wrapper sec-space'>";
        echo "<div class='container'>";
        echo "<div class='row'>";
    } else {
        echo "<section class='wrapper sec-space'>";
        echo "<div class='container'>";
    }
}

add_action('woocommerce_before_main_content', 'my_woocommerce_wrapper_start', 20);

//Adding WooCommerce Custom Before Wrapper
function my_woocommerce_wrapper_end()
{
    if (!is_product()) {
        echo "</div>";
        echo "</div>";
        echo "</section>";
    } else {
        echo "</div>";
        echo "</section>";
    }
}

add_action('woocommerce_after_main_content', 'my_woocommerce_wrapper_end', 10);

//Removing WooCommerce Breadcrumbs and Hooking Custom Breadcrumbs
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

function my_woocommerce_breadcrumbs()
{
    ?>
    <section class="breadcrumb-bg mask-overlay">
        <div class="container">
            <div class="site-breadcumb">
                <h1 class="title-1"> <?php woocommerce_page_title(); ?></h1>
                <ol class="breadcrumb breadcrumb-menubar">
                    <?php
                    $args = array(
                        'delimiter' => ' ',
                        'wrap_before' => '<li>',
                        'wrap_after' => '</li>'
                    );
                    woocommerce_breadcrumb($args); ?>
                </ol>
            </div>
        </div>
    </section>
<?php }

add_action('woocommerce_before_main_content', 'my_woocommerce_breadcrumbs', 10);
/**
 * Adding Breadcrumbs in cart Page
 */
add_action('woocommerce_before_cart', 'my_woocommerce_breadcrumbs', 5);

/**
 * Adding Breadcrumbs in empty cart Page
 */
add_action('woocommerce_cart_is_empty', 'my_woocommerce_breadcrumbs', 1);

/**
 * Adding Breadcrumbs in empty cart Page
 */
add_action('woocommerce_before_checkout_form', 'my_woocommerce_breadcrumbs', 1);

//Removing WooCommerce Archive and Product Description
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

//Adding Custom wrapper start on product
function my_woocommerce_product_wrapper_start()
{
    echo " <div class='col-md-9 col-sm-8 categories'>";
}

add_action('woocommerce_before_shop_loop', 'my_woocommerce_product_wrapper_start', 5);
add_action('woocommerce_no_products_found', 'my_woocommerce_product_wrapper_start', 5);

//Adding Custom wrapper start on product
function my_woocommerce_product_wrapper_end()
{
    echo " </div>";
}

add_action('woocommerce_after_shop_loop', 'my_woocommerce_product_wrapper_end', 20);
add_action('woocommerce_no_products_found', 'my_woocommerce_product_wrapper_end', 15);

//Adding Markup for product sort
function my_woocommerce_product_sort_start()
{
    echo "<div class='sorter-bar block-inline'>";
}

add_action('woocommerce_before_shop_loop', 'my_woocommerce_product_sort_start', 5);

function my_woocommerce_product_sort_end()
{
    echo "</div>";
}

add_action('woocommerce_before_shop_loop', 'my_woocommerce_product_sort_end', 40);

//Removing notice wrapper

remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);

//Adding and Ending Wrapper around ordering form
function my_woocommerce_catalog_ordering_wrapper_start()
{ ?>
    <div class="select-option">
<?php }

add_action('woocommerce_before_shop_loop', 'my_woocommerce_catalog_ordering_wrapper_start', 29);

function my_woocommerce_catalog_ordering_wrapper_end()
{ ?>
    </div>
<?php }

add_action('woocommerce_before_shop_loop', 'my_woocommerce_catalog_ordering_wrapper_end', 31);

//Adding list and grid button on woocommerce sort panel

function my_woocommerce_tab_button()
{ ?>
    <div class="tabs-btns">
        <ul class="tabination view-tabs">
            <li> View</li>
            <li class="active">
                <a href="#grid-view" data-toggle="tab">
                    <i class="fa fa-th"></i>
                </a>
            </li>
            <li class="">
                <a href="#list-view" data-toggle="tab">
                    <i class="fa fa-th-list" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
    </div>
    <?php
}

add_action('woocommerce_before_shop_loop', 'my_woocommerce_tab_button', 35);


//Removing thumbnail and adding as per design
function my_woocommerce_product_image_wrapper_start()
{
    echo "<div class='product-image'>";
}

add_action('woocommerce_before_shop_loop_item', 'my_woocommerce_product_image_wrapper_start', 5);

function my_woocommerce_product_image_wrapper_end()
{
    echo "</div>";
}

add_action('woocommerce_before_shop_loop_item', 'my_woocommerce_product_image_wrapper_end', 25);

remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

add_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_thumbnail', 15);

function my_woocommerce_hover_product_image()
{
    $attachment_id = get_field('product_hover_image');
    $size = "product-hover"; // (thumbnail, medium, large, full or custom size)
    $image = wp_get_attachment_image_src($attachment_id, $size);
    ?>
    <span class="product-hover">
     <img src="<?php echo $image[0]; ?>">
    </span>
<?php }

add_action('woocommerce_before_shop_loop_item', 'my_woocommerce_hover_product_image', 20);

//Custom title, price, rating and add to cart from Content Product

function my_woocommerce_product_content_wrapper_start()
{
    echo "<div class='product-content'>";
}

add_action('woocommerce_shop_loop_item_title', 'my_woocommerce_product_content_wrapper_start', 5);

function my_woocommerce_product_content_wrapper_end()
{
    echo "</div>";
}

add_action('woocommerce_after_shop_loop_item', 'my_woocommerce_product_content_wrapper_end', 15);

remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

function my_woocommerce_product_title()
{
    echo "<h2 class='product-title'>" . get_the_title() . "</h2>";
}

add_action('woocommerce_shop_loop_item_title', 'my_woocommerce_product_title', 10);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10);

function my_woocommerce_get_rating_html($rating, $count = 0)
{
    $html = 0 < $rating ? '<div class="rating">' . wc_get_star_rating_html($rating, $count) . '</div>' : '';
    return apply_filters('my_woocommerce_get_rating_html', $html, $rating, $count);
}


function my_woocommerce_add_to_cart_start()
{
    echo "<div class='product-links'>";
}

add_action('woocommerce_after_shop_loop_item', 'my_woocommerce_add_to_cart_start', 9);

function my_woocommerce_add_to_cart_end()
{
    echo "</div>";
}

add_action('woocommerce_after_shop_loop_item', 'my_woocommerce_add_to_cart_end', 11);


/////////////////////////Cart Updating via ajax//////////////////
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    ?>

    <a class="cart-customlocation">
        <i class="cart-icon icon-handbag icons"></i> <span
                class="count"> <?php echo sprintf(_n('%d', '%d', WC()->cart->get_cart_contents_count()), WC()->cart->get_cart_contents_count()); ?>
                                    </span>
    </a>

    <?php

    $fragments['a.cart-customlocation'] = ob_get_clean();

    return $fragments;

}

/////////////////////////Mini-Cart Updating via ajax//////////////////

add_filter('woocommerce_add_to_cart_fragments', function ($fragments) {

    ob_start();
    ?>

    <ol class="cart-list">
        <?php woocommerce_mini_cart(); ?>
    </ol>

    <?php $fragments['ol.cart-list'] = ob_get_clean();

    return $fragments;

});

//Min Cart Button Markup Changes
add_action('woocommerce_widget_shopping_cart_buttons', function () {
    // Removing Buttons
    remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10);
    remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20);

    // Adding customized Buttons
    add_action('woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_button_view_cart', 10);
    add_action('woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_proceed_to_checkout', 20);
}, 1);

// Custom cart button
function custom_widget_shopping_cart_button_view_cart()
{
    $original_link = wc_get_cart_url();
    echo '<div class="left"><a href="' . esc_url($original_link) . '" class="button wc-forward theme-btn-1 btn small-btn">' . esc_html__('View cart', 'woocommerce') . '</a></div>';
}

// Custom Checkout button
function custom_widget_shopping_cart_proceed_to_checkout()
{
    $original_link = wc_get_checkout_url();
    echo '<div class="right"><a href="' . esc_url($original_link) . '" class="button checkout wc-forward theme-btn btn small-btn">' . esc_html__('Checkout', 'woocommerce') . '</a></div>';
}

/////////////////////////////Login Functionality Woocommerce/////////////////////
add_filter('wp_nav_menu_items', 'add_loginout_link', 10, 2);

function add_loginout_link($items, $args)
{

    if (is_user_logged_in() && $args->theme_location == 'menu-2') {
        $items .= '<li style="font-weight:bold; text-transform:uppercase;"><a href="' . get_permalink(wc_get_page_id('myaccount')) . '">My Account</a></li>';
        $items .= '<li style="font-weight:bold; text-transform:uppercase;"><a href="' . wp_logout_url(get_permalink(wc_get_page_id('myaccount'))) . '">Log Out</a></li>';

    } elseif (!is_user_logged_in() && $args->theme_location == 'menu-2') {


        $items .= '<li style="font-weight:bold; text-transform:uppercase;"><a href="' . get_permalink(wc_get_page_id('myaccount')) . '"><i class="icon_lock-open"></i>Log In</a></li>';
        $items .= '<li style="font-weight:bold; text-transform:uppercase;"><a href="' . get_permalink(wc_get_page_id('myaccount')) . '">Register</a></li>';

    }

    return $items;

}


/////////////////////////////Hooking in Single Page about stock quantity/////////////////////

function my_woocommerce_stock_availability()
{
    global $product;
    $availability = $product->get_availability();
    if (!empty($availability['availability'])) {
        ?>
        <div class="product-availability">
            <ul class="stock-detail list-items black-color">
                <li class=""><i class="icon-layers icons"></i>
                    <span> <?php echo _e('Only'); ?> <b><?php echo $product->get_stock_quantity(); ?></b> <?php echo _e('Left'); ?> </span>
                    <i class="arrow_carrot-down"></i></li>
                <li><b><?php echo _e('Available'); ?>:</b> <span class="green-color">
                    <?php if ($product->get_stock_quantity() > 0) {
                        echo _e('In Stock');
                    } else {
                        echo _e('Out of Stock');
                    } ?>
                </span></li>
            </ul>
        </div>
        <hr class="divider-2">
    <?php } else { ?>
        <hr class="divider-2">
        <?php
    }
}

add_action('woocommerce_single_product_summary', 'my_woocommerce_stock_availability', 15);

//Removing In-Stock form default woocommerce
function my_woocommerce_hide_in_stock_message($html, $product)
{
    if ($product->is_in_stock()) {
        return '';
    }

    return $html;
}

add_filter('woocommerce_get_stock_html', 'my_woocommerce_hide_in_stock_message', 10, 2);

//Injecting markup after short description
function my_woocommerce_divider_from_short_description()
{
    global $post;
    $short_description = apply_filters('woocommerce_short_description', $post->post_excerpt);
    if (!empty($short_description)) {
        echo "<hr class='divider-2'>";
    } else {

    }
}

add_action('woocommerce_single_product_summary', 'my_woocommerce_divider_from_short_description', 21);

/**
 * Change number of related products output
 */
function woo_related_products_limit()
{
    global $product;

    $args['posts_per_page'] = 9;
    return $args;
}

add_filter('woocommerce_output_related_products_args', 'my_woocommerce_related_products_args');
function my_woocommerce_related_products_args($args)
{
    $args['posts_per_page'] = 9; // 4 related products
    $args['columns'] = 3; // arranged in 2 columns
    return $args;
}

/**
 * Remove Empty Cart Message
 */

remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);

/**
 * Remove Coupen, login and hook it to new hook at Checkout Page
 */
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);
add_action('my_woocommerce_custom_coupen', 'woocommerce_checkout_login_form', 5);

remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);
add_action('my_woocommerce_custom_coupen', 'woocommerce_checkout_coupon_form', 5);

/**
 * Remove Error and hook it to new hook form login page
 */

remove_action('woocommerce_before_customer_login_form', 'woocommerce_output_all_notices', 10);
add_action('my_woocommerce_login_notice', 'woocommerce_output_all_notices', 10);


/**
 * Remove Sale Flash
 */

remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
