<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package asperias
 */

/**
 * Display the menus
 */
function main_menu()
{

    $menu_args = array(
        'menu' => 'Primary',
        'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
        'container' => 'div',
        'container_id' => 'primary-navigation',
        'menu_class' => 'primary-navbar font-2',
        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
        'walker' => new WP_Bootstrap_Navwalker(),
    );

    echo wp_nav_menu($menu_args);

}
