<section class="main-slider">
    <div class="single-slider-1 owl-carousel owl-dots-1 owl-nav-1">
        <?php
        $i = 0;
        // check if the repeater field has rows of data
        if (have_rows('bannerabschnitt')):

            // loop through the rows of data
            while (have_rows('bannerabschnitt')) : the_row(); ?>
                <div class="item">
                    <img alt="slider" src="<?php the_sub_field('image'); ?>"/>
                    <div class="slide-info <?php if ($i == 2) {
                        echo 'right';
                    } ?>">
                        <div class="caption">
                            <h2 class="main-title"
                                data-animation-in="<?php the_sub_field('animation'); ?>"><?php echo strip_tags(get_sub_field('content'), '<span>'); ?></h2>
                            <div class="buttons" data-animation-in="rotateIn"><a class="btn btn-black"
                                                                                 href="<?php echo get_permalink(get_sub_field('link')); ?>">Jetzt
                                    Erkunden</a></div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
            endwhile;
        else :
            // no rows found

        endif;
        ?>
    </div>
</section>
