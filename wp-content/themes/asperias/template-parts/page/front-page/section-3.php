<section class="section-best">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="col-md-4 col-sm-4 col-xs-12 p-quality">
                    <div class=" pq-img">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/home-sixteen/parfume/icon10.png'; ?>">
                    </div>
                    <div class=" pq-content">
                        <h4>Extrait de Parfum</h4>
                        <p><?php the_sub_field('extrait_de_parfum'); ?></p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 p-quality">
                    <div class=" pq-img">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/home-sixteen/parfume/icon11.png'; ?>">
                    </div>
                    <div class=" pq-content">
                        <h4>Kaufe den Duft</h4>
                        <p><?php the_sub_field('kaufe_den_duft'); ?></p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 p-quality">
                    <div class=" pq-img">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/home-sixteen/parfume/icon12.png'; ?>">
                    </div>
                    <div class=" pq-content">
                        <h4>Extrem langhaltender Duft</h4>
                        <p><?php the_sub_field('extrem_langhaltender_duft'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
