<section class="features-parfume">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="block">
                    <div class="block-icon">
                        <i class="icon_comment_alt"></i>
                    </div>
                    <div class="block-content">
                        <h3 class="title-2">WhatsApp-Service</h3>
                        <p><?php echo get_sub_field('whatsapp'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="block">
                    <div class="block-icon">
                        <i class="icon_currency"></i>
                    </div>
                    <div class="block-content">
                        <h3 class="title-2">Kostenloser Versand</h3>
                        <p><?php echo get_sub_field('versand'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="block">
                    <div class="block-icon">
                        <i class="icon_phone"></i>
                    </div>
                    <div class="block-content">
                        <h3 class="title-2">Kompetente Duftberatung</h3>
                        <p>(<?php echo get_sub_field('duftberatung'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

