<section class="content-all ptb-70">
    <div class="container">
        <div class="title-wrap">
            <h2 class="section-title" style="margin-top: 0px; padding-bottom: 15px;">
                <span class="gray-color"> <?php the_sub_field('uberschrift'); ?>
            </h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <div class="content-left">
                    <img src="<?php echo get_sub_field('spalte_1_image'); ?>">
                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('spalte_1_content')):

                        // loop through the rows of data
                        while (have_rows('spalte_1_content')) : the_row(); ?>
                            <div class="content-part">
                                <?php the_sub_field('content'); ?>
                            </div>
                        <?php endwhile;
                    else :
                        // no rows found
                    endif;
                    ?>
                </div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <div class="content-left">

                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('spalte_2_content')):

                        // loop through the rows of data
                        while (have_rows('spalte_2_content')) : the_row(); ?>
                            <div class="content-part">
                                <?php the_sub_field('content'); ?>
                            </div>
                        <?php endwhile;
                    else :
                        // no rows found
                    endif;
                    ?>
                    <img src="<?php echo get_sub_field('spalte_2_image'); ?>">
                </div>
            </div>

            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <div class="content-left">
                    <img src="<?php echo get_sub_field('spalte_3_image'); ?>">
                    <h4>ÜBER PARFÜMS</h4>
                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('spalte_3_content')):

                        // loop through the rows of data
                        while (have_rows('spalte_3_content')) : the_row(); ?>
                            <div class="content-part">
                                <?php the_sub_field('content'); ?>
                            </div>
                        <?php endwhile;
                    else :
                        // no rows found
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
