<!-- Features -->
<section class="features ptb-70">
    <div class="container">
        <div class="title-wrap">
            <h2 class="section-title"><span class="gray-color"> welcome </span> to Asperias Parfum </h2>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-4 col-lg-4">
                <div class="block">
                    <?php if (get_sub_field('linke_content')) : ?>
                        <div class="block-content-left">
                            <?php the_sub_field('linke_content'); ?>
                            <h3 class="title-2"><a
                                        href="<?php echo get_permalink(get_sub_field('mehr_erfahren_link')); ?>"
                                        class="color"> Mehr
                                    erfahren <span class="arrow_right"></span>
                                </a></h3>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8  col-lg-8 centered">
                <div class="block">
                    <div class="image">
                        <?php if (get_field('rechts_image')) : ?>
                            <img src="<?php echo get_sub_field('rechts_image'); ?>"/>
                        <?php else: ?>
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/home-sixteen/parfume/asperias-men.jpg'; ?>"/>
                        <?php endif; ?>
                    </div>
                    <div class="block-content float-r-block">
                        <?php the_sub_field('rechts_content', false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Features -->
