<?php

$bestseller_product = get_sub_field('bestseller_shortcode'); ?>

<!-- Our Products Starts -->
<section class="our-products">
    <div class="container">
        <div class="title-wrap carousal-title">
            <h2 class="main-title"> Bestseller der Season </h2>
        </div>
        <div class="prod-block">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="prod-tab-1">
                    <div class="prod-slider-1 owl-carousel owl-nav-2">
                        <?php echo do_shortcode($bestseller_product); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
