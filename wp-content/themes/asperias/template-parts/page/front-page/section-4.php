<!-- Hot Deal -->
<section class="hot-deal ptb-70" style="background-image: url(<?php echo get_sub_field('hintergrundbild'); ?>);">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="content">
                    <h2 class="main-title"><?php the_sub_field('titel'); ?></h2>
                    <p><?php the_sub_field('inhalt'); ?></p>
                    <div class="buttons"><a class="btn theme-btn"
                                            href="<?php echo get_permalink(get_sub_field('link')); ?>">Explore
                            now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 parfume-right-img">
                <img src="<?php echo get_sub_field('rechtes_bild'); ?>"/>
            </div>
        </div>
    </div>
</section>
<!-- / Hot Deal -->
