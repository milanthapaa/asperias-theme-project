<?php
/**
 * The Template for displaying subcategories of parent category
 */


get_header('shop');
?>

<!--Breadcrumb Section Start-->
<section class="breadcrumb-bg mask-overlay">
    <div class="container">
        <div class="site-breadcumb">
            <h1 class="title-1"><?php woocommerce_page_title(); ?></h1>
            <ol class="breadcrumb breadcrumb-menubar">
                <!--                    <li><a href="#"> Home </a> <a href="#"> Shop </a> Shop Grid with Sidebar Left</li>-->
                <?php
                $args = array(
                    'delimiter' => ' ',
                    'wrap_before' => '<li>',
                    'wrap_after' => '</li>'
                );
                woocommerce_breadcrumb($args); ?>
            </ol>
        </div>
    </div>
</section>
<!--Breadcrumb Section End-->

<section class="wrapper sec-space">
    <!-- Single Product Starts -->
    <div class="container">
        <div class="row">
            <!-- Products Sidebar Starts -->
            <aside class="prod-sidebar col-sm-4 col-md-3">
                <div class="widget-wrap widget-wrap-aside">
                    <h2 class="widget-title"> Bewertungen </h2>
                </div>
                <div class="ts-rating-light ts-rating de_DE testimonial">
                    <div class="carousel slide paused" id="ts-carousel" data-ride="tb-sticker-carousel"
                         style="background-color: rgb(255, 220, 15);">
                        <div class="carousel-header">
                            <div class="review-header">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="carousel-inner">
                            <div class="item sideItem">
                                <div class="review">Alles gut und zuverlässig</div>
                                <div class="reviewer">
                                    <div class="reviewer-name">Brigitte H., Viechtach</div>
                                    <div class="ts-date-info">Datum der Veröffentlichung: 07.01.2019<br>Datum der
                                        Kauferfahrung: 27.12.2018
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <div class="item sideItem">
                                <div class="review">Sehr zu empfehlen. Ich bestelle bald wieder.</div>
                                <div class="reviewer">
                                    <div class="reviewer-name"></div>
                                    <div class="ts-date-info">Datum der Veröffentlichung: 07.01.2019<br>Datum der
                                        Kauferfahrung: 03.01.2019
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <div class="item sideItem active">
                                <div class="review">Hammer Parfum wie das oreginal Würde jeder Zeit wieder bei euch
                                    bestellen
                                </div>
                                <div class="reviewer">
                                    <div class="reviewer-name">Litz E., Bremen</div>
                                    <div class="ts-date-info">Datum der Veröffentlichung: 07.01.2019<br>Datum der
                                        Kauferfahrung: 03.01.2019
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <div class="item sideItem">
                                <div class="review">War skeptisch ,bin aber begeistert da es genau so duftet wie das
                                    Original..
                                </div>
                                <div class="reviewer">
                                    <div class="reviewer-name"></div>
                                    <div class="ts-date-info">Datum der Veröffentlichung: 07.01.2019<br>Datum der
                                        Kauferfahrung: 02.01.2019
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <div class="item sideItem">
                                <div class="review" alt="" title="">
                                    Ich habe women 082 auf die Duftbeschreibung hin bestellt und bin mehr als zufrieden.
                                    Der Duft ist ...
                                </div>
                                <div class="reviewer">
                                    <div class="reviewer-name">Nicole D., Uetze</div>
                                    <div class="ts-date-info">Datum der Veröffentlichung: 07.01.2019<br>Datum der
                                        Kauferfahrung: 03.01.2019
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                            <ol class="carousel-indicators">
                                <li class="" data-target="#ts-carousel" data-slide-to="0"></li>
                                <li class="" data-target="#ts-carousel" data-slide-to="1"></li>
                                <li class="active" data-target="#ts-carousel" data-slide-to="2"></li>
                                <li class="" data-target="#ts-carousel" data-slide-to="3"></li>
                                <li class="" data-target="#ts-carousel" data-slide-to="4"></li>
                            </ol>
                            <div class="ratings-total">
                                <a href="https://www.trustedshops.de/bewertung/info_XCD5139D9D9DC41B03CDD95B298A5DEF3.html"
                                   target="_blank" style="color: rgb(0, 0, 0); font-family: Arial;">936 Bewertungen</a>
                            </div>
                        </div>
                    </div>
                    <div class="ts-footer">
                        <div class="footerArrow"
                             style="border-color: transparent rgb(255, 220, 15) transparent transparent;">
                        </div>
                    </div>
                    <div class="ts-footer-logo-font">
                        <i class="ts-icon ts-e-trustedshops"></i>
                    </div>
                </div>
            </aside>
            <!-- Products Sidebar Ends -->

            <!-- Products Categories Starts -->
            <div class="col-md-9 col-sm-8 categories cat-menu">
                <div class="sorter-bar block-inline block-inline-aside">
                    <div class="show-result font-2">
                        <h2 class="widget-title"><?php woocommerce_page_title(); ?></h2>
                    </div>
                </div>

                <div class="tab-content">
                    <!-- Product Grid View Starts -->
                    <div id="grid-view" class="tab-pane fade active in" role="tabpanel">
                        <?php
                        $cate = get_queried_object();
                        $cateID = $cate->term_id;

                        $args = array(
                            'order' => 'ASC',
                            'hierarchical' => 1,
                            'show_option_none' => '',
                            'hide_empty' => 0,
                            'parent' => $cateID,
                            'taxonomy' => 'product_cat',
                        );
                        $subcats = get_categories($args);
                        $i = 1;
                        echo "<div class='row'>";
                        foreach ($subcats as $sc) {
                            $link = get_term_link($sc->slug, $sc->taxonomy);
                            $thumbnail_id = get_term_meta($sc->term_id, 'thumbnail_id', true);
                            $image_url = wp_get_attachment_url($thumbnail_id); //
                            ?>
                            <div class="col-sm-3 col-lg-3 col-xs-12">
                                <div class="product-item cat-item">
                                    <div class="product-image">
                                        <a href="<?php echo $link; ?>" class="img">
                                            <img src="<?php echo "$image_url"; ?>"/>
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <h2 class="product-title"><a title="<?php echo $sc->name; ?>"
                                                                     href="<?php echo $link; ?>"><?php echo $sc->name; ?></a>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($i % 4 == 0) {
                                echo '</div><div class="row">';
                            }
                            $i++;
                        } ?>
                    </div>
                </div>
                <!-- Product Grid View Ends -->
            </div>
        </div>
        <!-- Products Description Starts -->
    </div>
    </div>
    <!-- / Single Product Ends -->
</section>
<?php get_footer('shop'); ?>
