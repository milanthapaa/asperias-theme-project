<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package asperias
 */

get_header();
?>

    <article class="page-body">

        <section class="wrapper sec-space 404-error">
            <div class="container">
                <!-- Error Starts -->
                <div class="error-wrap text-center space-bottom-70">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/common/error.png'; ?>" alt="">
                    <h2 class="main-title">SEITE NICHT GEFUNDEN</h2>
                    <p class="size-16 space-30">DIE VON IHNEN ANGEFORDERTE SEITE IST NICHT MEHR VORHANDEN</p>
                    <a href="<?php echo get_home_url(); ?>">
                        <button type="button" class="btn error-botton"><i class="fa fa-angle-left"></i> Startseite
                        </button>
                    </a>
                </div>
            </div>
        </section>

    </article>

<?php
get_footer();
