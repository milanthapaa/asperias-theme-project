<?php
/**
 * Loop through seiteninhalt sections registered in ACF plugin. Field group -> "seiteninhalt Sections" displaying front page
 *
 */
?>
<?php get_header();

if (have_rows('seiteninhalt')):
    while (have_rows('seiteninhalt')): the_row();

        //Banner Section
        if (get_row_layout() == 'banner-schieberegler'):

            get_template_part('template-parts/page/front-page/section', 'banner');

        elseif (get_row_layout() == 'sektion_1'):

            get_template_part('template-parts/page/front-page/section', '1');

        elseif (get_row_layout() == 'sektion_2'):

            get_template_part('template-parts/page/front-page/section', '2');

        elseif (get_row_layout() == 'sektion_3'):

            get_template_part('template-parts/page/front-page/section', '3');

        elseif (get_row_layout() == 'sektion_4'):

            get_template_part('template-parts/page/front-page/section', '4');

        elseif (get_row_layout() == 'sektion_5'):

            get_template_part('template-parts/page/front-page/section', '5');

        elseif (get_row_layout() == 'sektion_6'):

            get_template_part('template-parts/page/front-page/section', '6');

        endif;

    endwhile;
else :
    get_template_part('template-parts/page/content', '');
endif;

get_footer(); ?>
