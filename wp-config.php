<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asperias');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'w?!W7n$WxZe{Iv[?D#6D4D#*Opj`yeg#9P8RmUb7Lvgmb@j!x!KM#V{l$LA~*v<#');
define('SECURE_AUTH_KEY',  'z^R!C^Q^gaX70h/ s8aH!+@icnYI<idfY^9b`wL<}K.bIak.&_+q#%%.;gT6_^-_');
define('LOGGED_IN_KEY',    '=~H=dd?At-lO:HQ(}o&ayQ!<qw${r}lQ[yvN:8UxtkxUw9n@Eb>Uvyj%:fy-nHsI');
define('NONCE_KEY',        'LN#sdb.$H=|}}C>?Tl|Rz{bM/q[t1|?o l CE/=kPYl+o%jAK^}MLJdwok$)y12_');
define('AUTH_SALT',        'o*z&O),u})Nbc1U4&b}itJ/Gb^p0CJgcjXHgT|(}$e6C,rMx}<TsQSr:L*Nnw_?a');
define('SECURE_AUTH_SALT', 'JFhgk_[J4^+7etHW2b] Tc9Jzjn!ESf-)qi?BJ;u5?yk{]bmt)H}|W%X1:35x5E,');
define('LOGGED_IN_SALT',   'P.`MHv1uR:_VKP1@YZ,D%#xY6HnqNS2/[-tRr?P`V 76XQaAi37wK+X>{dA+c][%');
define('NONCE_SALT',       'jt}?>&;O7/Q=V&WE5lL-y}[5/1TNeTk-0QI#v;/0Uf>+M@.Fwo;6X?Qij7)|wf&Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
